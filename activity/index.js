function Pokemon(name, lvl, hp){
		
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;
	
		this.tackle = function(target){

			console.log(`${this.name} tackled ${target.name}, dealing ${this.attack} damage`);

			target.health = target.health - this.attack;

			faint()
			function faint() {
				if (target.health <= 0) {
					console.log(`${target.name}'s health is down to ${target.health}\n${target.name} fainted`);
				} else {
					console.log(`${target.name}'s health is down to ${target.health}`)
				}
			}

		};
	};

	let charizard = new Pokemon("Charizard", 5, 50);
	let blastoise = new Pokemon("Blastoise", 5, 25);

	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
	charizard.tackle(blastoise);
